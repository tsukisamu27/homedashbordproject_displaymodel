from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base
import logging
import logging.handlers
from pathlib import Path

def getSqlLogger():
    FORM = "%(asctime)s:	%(threadName)s:%(levelname)s:%(message)s"
    FILENAME = "SQL.log"
    SAVEPATH = Path.cwd() / Path(__file__).parents[0] / "log"

    logger = logging.getLogger('sqlalchemy')
    fh = logging.handlers.RotatingFileHandler(SAVEPATH / FILENAME, maxBytes=1000**2, backupCount=3)
    fh.setLevel(logging.DEBUG)
    fh_formatter = logging.Formatter(FORM)
    fh.setFormatter(fh_formatter)
    logger.addHandler(fh)
    logger.propagate = False

# Sessionの作成
DB_NAME = 'dashbord'
DB_USER = '<DB_USER>'
DB_PASSWORD = '<PASSWORD>'
DB_HOST = '<IP_ADDRESS>'

DB_URI = 'postgresql://{}:{}@{}/{}'.format(DB_USER,DB_PASSWORD, DB_HOST, DB_NAME)

engine = create_engine(
            DB_URI, 
            #pool_pre_ping=True, 
            echo=True
            )

session_factory = sessionmaker(
            autocommit = False,
            autoflush = True,
            bind = engine
            )

Session = scoped_session(session_factory)
                
# modelで使用する
Base = declarative_base()

# getSqlLogger()