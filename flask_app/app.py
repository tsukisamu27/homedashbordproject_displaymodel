from flask import Flask, request, jsonify, render_template
from flask_marshmallow import Marshmallow

from dbSetting import session_factory
from flask_sqlalchemy_session import flask_scoped_session

app = Flask(__name__,
            static_folder = "static",
            template_folder = "templates")

app.config['JSON_AS_ASCII'] = False
session = flask_scoped_session(session_factory, app)
ma = Marshmallow(app)

@app.route('/')
def hello():
    name = "Hello World"
    return name

@app.route('/index')
def index():
    return render_template('index.html')

from controllers.amedasu import amedasu
app.register_blueprint(amedasu)

from controllers.forecast import forecast
app.register_blueprint(forecast)

from controllers.co2mini import co2mini
app.register_blueprint(co2mini)

from controllers.switchbot import switchbot
app.register_blueprint(switchbot)

from controllers.smartplug import smartplug
app.register_blueprint(smartplug)

from controllers.dashbordapi import dashbord
app.register_blueprint(dashbord)

from controllers.yahoorss import yahoorss
app.register_blueprint(yahoorss)

from controllers.traininfo import traininfo
app.register_blueprint(traininfo)

if __name__ == "__main__":
    app.run(host='localhost',
            port=5000,
            threaded=True,
            debug=True)
            