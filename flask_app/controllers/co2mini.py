from flask import jsonify, Blueprint
from models import *
from app import session
from datetime import datetime, timedelta

co2mini = Blueprint('co2mini', __name__, template_folder='templates', static_folder='./static')

def checkValue(outputDict,key):
  if key in outputDict:
    return outputDict[key]
  else:
    return '---'

def CO2miniFormat(output):
  return {
    'co2':{'name':'co2','unit':'ppm','value':checkValue(output,'co2')}
    #'temperature':{'name':'温度','unit':'℃','value':output['temperature']},
    #'createAt':{'name':'createAt','unit':'','value':output['createAt']}
    }

def getCO2mini():
  monitor = session.query(CO2mini).\
      order_by(CO2mini.createAt.desc()).\
      filter(CO2mini.createAt >= datetime.now() - timedelta(minutes=1)).\
      first()
  monitor_schema = CO2miniSchema(many=False)
  output = monitor_schema.dump(monitor)
  sutatus = 'none' if monitor is None else 'ok'
  return sutatus,output

@co2mini.route("/api/co2mini", methods=["GET"])
def dbCO2mini():
  sutatus, output = getCO2mini()
  return jsonify({'co2mini': {'status':sutatus,'result':output}})