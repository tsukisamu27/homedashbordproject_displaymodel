from flask import jsonify, Blueprint
from models import *
from app import session
from libs import *
from datetime import datetime, timedelta

amedasu = Blueprint('amedasu', __name__, template_folder='templates', static_folder='./static')

def checkValue(outputDict,key):
  if key in outputDict:
    return outputDict[key]
  else:
    return '---'

def amedasuFormat(output):
  return {
    'createAt':{'name':'取得日','unit':'','value':checkValue(output,'createAt')},
    'humidity':{'name':'湿度','unit':'%','value':checkValue(output,'humidity')},
    'normalPressure':{'name':'海面気圧','unit':'hPa','value':checkValue(output,'normalPressure')},
    'sum1h':{'name':'日照時間','unit':'h','value':checkValue(output,'sum1h')},
    'temp':{'name':'気温','unit':'℃','value':checkValue(output,'temp')},
    'wind':{'name':'風速','unit':'m/s','value':checkValue(output,'wind')},
    'windDirection':{'name':'風向','unit':'','value':checkValue(output,'windDirection')}
    }

def getAmedasu():
  monitor = session.query(Amedasu).\
      order_by(Amedasu.createAt.desc()).\
      filter(Amedasu.createAt >= datetime.now() - timedelta(minutes=1)).\
      first()
  monitor_schema = AmedasuSchema(many=False)
  output = monitor_schema.dump(monitor)
  sutatus = 'none' if monitor is None else 'ok'
  return sutatus,output

@amedasu.route("/api/amedasu", methods=["GET"])
def dbAmedasu():
  sutatus, output = getAmedasu()
  return jsonify({'amedasu': {'status':sutatus,'result':output}})