from flask import jsonify, Blueprint
from models import *
from app import session
from datetime import datetime, timedelta

smartplug = Blueprint('smartplug', __name__, template_folder='templates', static_folder='./static')

def checkValue(outputDict,key):
  if key in outputDict:
    return outputDict[key]
  else:
    return '---'

def smartplugFormat(output):
  return {
    'Current':{'name':'Watt','unit':'W','value':checkValue(output,'Current')},
    'Power':{'name':'Power','unit':'A','value':checkValue(output,'Power')},
    'Voltage':{'name':'Voltage','unit':'V','value':checkValue(output,'Voltage')},
    #'createAt':{'name':'createAt','unit':'','value':output['createAt']}
    #'deviceId':{'name':'deviceId','unit':'','value':output['deviceId']}
    }

def getSmartplug(deviceId):
  monitor = session.query(Smartplug).\
      filter(Smartplug.deviceId==deviceId).\
      filter(CO2mini.createAt >= datetime.now() - timedelta(minutes=1)).\
      order_by(Smartplug.createAt.desc()).\
      first()
  monitor_schema = SmartplugSchema(many=False)
  output = monitor_schema.dump(monitor)
  sutatus = 'none' if monitor is None else 'ok'
  return sutatus,output

@smartplug.route("/api/smartplug", methods=["GET"])
def dbSmartplug():
  sutatus, output = getSmartplug("678013645002911999fb")
  return jsonify({'smartplug': {'status':sutatus,'result':output}})