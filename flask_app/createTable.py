from models import traininfo
from dbSetting import engine
from models import *

Base.metadata.create_all(
    bind=engine, 
    tables=[TrainInfo.__table__],
    checkfirst=False
    )