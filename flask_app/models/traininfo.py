from sqlalchemy import Column,String,DateTime
from flask_marshmallow import Marshmallow
from dbSetting import Base

ma = Marshmallow()

class TrainInfo(Base):
    __tablename__ = 'traininfo'
    createAt = Column(DateTime,primary_key=True)
    Date     = Column(String)
    Time     = Column(String)
    Name     = Column(String,primary_key=True)
    Status   = Column(String)
    Comment  = Column(String)

class TrainInfoSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TrainInfo
        load_instance = True