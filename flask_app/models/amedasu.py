from sqlalchemy import Column, Integer, String, Float, DateTime
from flask_marshmallow import Marshmallow
from dbSetting import Base

ma = Marshmallow()

class Amedasu(Base):
    __tablename__ = 'amedasu'
    createAt         = Column(DateTime, primary_key=True)
    pressure         = Column(Float)
    normalPressure   = Column(Float)
    temp             = Column(Float)
    humidity         = Column(Float)
    sun10m           = Column(Float)
    sum1h            = Column(Float)
    precipitation10m = Column(Float)
    precipitation1h  = Column(Float)
    precipitation3h  = Column(Float)
    precipitation24h = Column(Float)
    windDirection    = Column(Integer)
    wind             = Column(Float)

class AmedasuSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Amedasu
        load_instance = True