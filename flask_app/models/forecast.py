from sqlalchemy import Column, Integer, String, Float, DateTime
from flask_marshmallow import Marshmallow
from dbSetting import Base

ma = Marshmallow()

class Forecast(Base):
    __tablename__ = 'forecast'
    createAt= Column(DateTime, primary_key=True)
    timeDefine= Column(String,primary_key=True)
    publishingOffice= Column(String)
    reportDatetime= Column(String)
    weatherCode= Column(String)
    weather= Column(String)
    wind= Column(String)
    wave= Column(String)
    pops= Column(String)
    reliabilities= Column(String)
    tempsMin= Column(String)
    tempsMinUpper= Column(String)
    tempsMinLower= Column(String)
    tempsMax= Column(String)
    tempsMaxUpper= Column(String)
    tempsMaxLower= Column(String)

class ForecastSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Forecast
        load_instance = True
        