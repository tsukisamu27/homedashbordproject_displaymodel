import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

import tinytuya
from datetime import datetime
from models import *
from baseComp import BaseComp
import time

class CollectSmartplugData():
    """
    SmartPlugのsensorからデータを取得してくるクラス
    """
    def __init__(self, device_id, ip_address, local_key, version):
        self.device_id  = device_id
        self.ip_address = ip_address
        self.local_key  = local_key
        self.version    = version
        self.outletDevice = self.getOutletDevice()

    def getOutletDevice(self):
        d = tinytuya.OutletDevice(self.device_id, self.ip_address, self.local_key)
        d.set_version(self.version)
        return d

class SmartPlugSensor(BaseComp):
    def __init__(self):
        super().__init__()
        self.filename = os.path.basename(__file__.split(".")[0])
        self.baseInterval = 2

    def doOnce(self):
        # センサーデータ取得
        self.data = []
        self.data.append(CollectSmartplugData('<device_id>', '<ip_address>', '<local_key>', 3.3))
        self.data.append(CollectSmartplugData('<device_id>', '<ip_address>', '<local_key>', 3.3))
    
    def loopBody(self):
        for d in self.data:
            sensorData = d.outletDevice.status()
            if 'dps' in sensorData and 'devId' in sensorData:
                addData=Smartplug(createAt = datetime.now(),
                        deviceId = sensorData['devId'],
                        Current  = sensorData['dps']['18'],
                        Power    = sensorData['dps']['19'],
                        Voltage  = sensorData['dps']['20']/10)
                self.storageData(addData)

if __name__ == "__main__":
    SmartPlugSensor().run()