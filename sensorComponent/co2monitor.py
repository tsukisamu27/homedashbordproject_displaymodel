import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

from datetime import datetime
from models import *
from baseComp import BaseComp
from CO2Meter import *

class Co2MonitorSensor(BaseComp):
    def __init__(self):
        super().__init__()
        self.filename = os.path.basename(__file__.split(".")[0])
        self._name = self.filename
        self.baseInterval = 2

    def doOnce(self):
        self.sensor = CO2Meter("/dev/hidraw0")
    
    def loopBody(self):
        try:
            self.sensor
        except:
            self.log.error("センサーとの疎通に失敗しました。再接続を試みます。")
            self.sensor = CO2Meter("/dev/hidraw0")

        if 'temperature' in self.sensor.get_data() and 'co2' in self.sensor.get_data():
            addData=CO2mini(createAt    = datetime.now(),
                            co2         = self.sensor.get_data()['co2'],
                            temperature = self.sensor.get_data()['temperature'])
            self.storageData(addData)
        else:
            self.log.error("データの取得が一時的に失敗しました")

if __name__ == "__main__":
    Co2MonitorSensor().run()
