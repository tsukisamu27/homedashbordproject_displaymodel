import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '../flask_app'))

from datetime import datetime
from models import *
from baseComp import BaseComp

import requests
from bs4 import BeautifulSoup
    
class TrainInfoApi(BaseComp):
    def __init__(self):
        super().__init__()
        self.filename = os.path.basename(__file__.split(".")[0])
        self._name = self.filename
        self.baseInterval = 300
    
    def getTrainInfoList(self):
        # getTableHtml
        user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36'
        header = {
            'User-Agent': user_agent
        }
        URL = "https://www.jorudan.co.jp/unk/"
        res = requests.get(URL,headers=header)
        soup = BeautifulSoup(res.text,"html.parser")
        table = soup.find("table", attrs={"class":"unktable"})

        # html情報を1行ごとに分割
        htmlRowlist = []
        for row in table.find_all("tr"):
            rowtd = row.find_all("td")
            if rowtd:
                htmlRowlist.append(rowtd)

        # row情報をデータごとに分割
        strRowlist = []
        for row in htmlRowlist:
            rowtext = []
            rowtext.extend(row[0].get_text(',').split(','))
            rowtext.extend(row[1].get_text(',').split(','))
            rowtext.extend(row[2])
            strRowlist.append(rowtext)

        return strRowlist

    def loopBody(self):
        infos = self.getTrainInfoList()
        print(infos)
        with self.session_scope() as session:
            #droptable
            self.session.query(TrainInfo).delete()
            #insert
            for info in infos:
                addData = TrainInfo(
                    createAt= datetime.now(),
                    Date= info[0],
                    Time= info[1],
                    Name= info[2],
                    Status =info[3],
                    Comment=info[4]
                )
                session.add(addData)
        
if __name__ == "__main__":
    TrainInfoApi().run()
